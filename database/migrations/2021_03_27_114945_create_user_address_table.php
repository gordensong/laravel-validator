<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_address', function (Blueprint $table) {
			$table->id();
			$table->integer('user_id')->nullable(false)->default(0);
			$table->string('province')->default('')->comment('省');
			$table->string('city')->default('')->comment('市');
			$table->string('district')->default('')->comment('区');
			$table->string('address', 255)->default('')->comment('详细地址');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_address');
	}
}
