<?php


namespace GordenSong\Laravel\Support;


use GordenSong\Laravel\Utils\RuleUtil;
use Webmozart\Assert\Assert;

abstract class TableValidator extends Validator
{
	/** @var null|string */
	protected $connection = null;

	/** @var string */
	protected $table = null;

	/**
	 * @throws \Doctrine\DBAL\Schema\SchemaException
	 * @throws \GordenSong\Laravel\Exceptions\TableNotExistException
	 */
	public function __construct()
	{
		$this->rules = RuleUtil::mergeManyRules($this->tableRules(), $this->rules());

		parent::__construct();
	}

	public function getTable(): string
	{
		Assert::notNull($this->table);

		return $this->table;
	}

	public function getConnection(): ?string
	{
		if (empty($this->connection)) {
			$this->connection = config('database.default');
		}

		Assert::notNull($this->connection);

		return $this->connection;
	}

	/**
	 * @throws \GordenSong\Laravel\Exceptions\TableNotExistException
	 * @throws \Doctrine\DBAL\Schema\SchemaException
	 */
	public function tableRules(): array
	{
		return RuleRepository::getRules($this->getConnection(), $this->getTable());
	}
}
