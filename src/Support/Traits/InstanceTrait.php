<?php

namespace GordenSong\Laravel\Support\Traits;

trait InstanceTrait
{
	public static function instance()
	{
		return new static();
	}
}