<?php

namespace GordenSong\Laravel\Support;

use GordenSong\Laravel\Support\Traits\InstanceTrait;
use GordenSong\Laravel\Support\Traits\PrefixTrait;
use GordenSong\Laravel\Support\Traits\SceneTrait;
use GordenSong\Laravel\Support\Traits\ValidatorTrait;
use GordenSong\Laravel\Utils\RuleUtil;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Traits\Macroable;

abstract class Validator implements Arrayable
{
	use ValidatorTrait, SceneTrait, PrefixTrait, InstanceTrait, Macroable;

	public function __construct()
	{
		if (method_exists($this, 'tableRules')) {
			$this->rules = RuleUtil::mergeManyRules($this->rules, $this->tableRules());
		}
		$this->rules = RuleUtil::mergeManyRules($this->rules, $this->customizeRules());
		$this->rules = RuleUtil::diffManyRules($this->rules, $this->excludeRules());

		$this->normalizeScenes();
	}

	public function toArray(): array
	{
		return [
			'rules' => $this->rules(),
			'messages' => $this->messages(),
			'attributes' => $this->attributes(),
		];
	}
}
