<?php


namespace GordenSong\Laravel\Support;


use GordenSong\Laravel\Utils\TableMeta;
use Illuminate\Support\Facades\Log;
use Webmozart\Assert\Assert;

class RuleRepository
{
	/**
	 * @throws \GordenSong\Laravel\Exceptions\TableNotExistException
	 * @throws \Doctrine\DBAL\Schema\SchemaException
	 */
	public static function getRules(string $connection, string $table): array
	{
		Assert::notNull($table);
		Assert::notNull($connection);

		Log::channel('stack')->info("rule from db $connection, $table");
		return TableMeta::make($connection, $table)->getRules();
	}
}