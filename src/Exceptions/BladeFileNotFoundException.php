<?php


namespace GordenSong\Laravel\Exceptions;


use Throwable;

class BladeFileNotFoundException extends \Exception
{
	public function __construct($filename, $code = 0, Throwable $previous = null)
	{
		parent::__construct('Blade file not found: ' . $filename, $code, $previous);
	}
}