<?php

namespace GordenSong\Laravel\Exceptions;

use Exception;
use Throwable;

class ValidatorException extends Exception
{
	public function __construct($message = "Validator Exception", $code = 0, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}