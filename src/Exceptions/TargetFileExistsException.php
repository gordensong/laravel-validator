<?php


namespace GordenSong\Laravel\Exceptions;


use Throwable;

class TargetFileExistsException extends \Exception
{
	public function __construct($filename, $code = 0, Throwable $previous = null)
	{
		parent::__construct('Target file exists: ' . $filename, $code, $previous);
	}
}