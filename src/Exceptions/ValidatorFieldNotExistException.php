<?php

namespace GordenSong\Laravel\Exceptions;

use Throwable;

class ValidatorFieldNotExistException extends ValidatorException
{
	public function __construct($field, $code = 0, Throwable $previous = null)
	{
		parent::__construct("validator field(`$field`) not exist", $code, $previous);
	}
}