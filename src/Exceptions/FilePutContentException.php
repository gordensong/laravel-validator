<?php


namespace GordenSong\Laravel\Exceptions;


use Throwable;

class FilePutContentException extends \Exception
{
	public function __construct($filename, $code = 0, Throwable $previous = null)
	{
		parent::__construct('File put content error. Filename: ' . $filename, $code, $previous);
	}
}