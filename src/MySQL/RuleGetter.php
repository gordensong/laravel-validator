<?php

namespace GordenSong\Laravel\MySQL;

use Doctrine\DBAL\Schema\Column;

interface RuleGetter
{
	public function rules(Column $column): array;
}