<?php


namespace GordenSong\Laravel\MySQL;


use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\DecimalType;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\JsonType;
use Doctrine\DBAL\Types\SmallIntType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;
use Doctrine\DBAL\Types\TimeType;
use RuntimeException;

class RuleGetterFactory
{
	private static $rules = [
		BooleanType::class => Rules\BooleanType::class,
		SmallIntType::class => Rules\SmallIntType::class,
		IntegerType::class => Rules\IntegerType::class,
		BigIntType::class => Rules\BigIntType::class,
		FloatType::class => Rules\FloatType::class,
		DecimalType::class => Rules\DecimalType::class,
		DateType::class => Rules\DateType::class,
		TimeType::class => Rules\TimeType::class,
		DateTimeType::class => Rules\DateTimeType::class,
		StringType::class => Rules\StringType::class,
		TextType::class => Rules\TextType::class,
		JsonType::class => Rules\JsonType::class,
	];

	/**
	 * @param Column $column
	 * @return \GordenSong\Laravel\MySQL\RuleGetter
	 */
	public static function make(Column $column): RuleGetter
	{
		$type = $column->getType();

		$rule = data_get(self::$rules, get_class($type));
		if (empty($rule)) {
			throw new RuntimeException('unknown rule type');
		}

		return new $rule;
	}
}
