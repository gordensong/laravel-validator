<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class TextType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$rules = ['string'];
		if ($column->getLength()) {
			$rules[] = 'max:' . $column->getLength();
		}
		return $rules;
	}
}