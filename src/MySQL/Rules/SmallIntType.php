<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class SmallIntType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$rules = ['integer'];

		if ($column->getUnsigned()) {
			$rules[] = 'min:0';
			$rules[] = 'max:65535';
		} else {
			$rules[] = 'min:-32768';
			$rules[] = 'max:32767';
		}

		return $rules;
	}
}