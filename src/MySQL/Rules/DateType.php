<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class DateType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$schemaType = $this->getSchemaType($column);

		$rules = [];
		if (strpos($schemaType, 'year') !== false) {
			$rules[] = 'integer';
			$rules[] = 'between:1901,2155';
		} elseif ($schemaType == 'date') {
			$rules[] = 'date';
		}

		return $rules;
	}
}