<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class StringType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$schemaType = $this->getSchemaType($column);

		$rules[] = 'string';
		if (strpos($schemaType, 'enum(') !== false) {

		} elseif (strpos($schemaType, 'set(') !== false) {

		} else {
			if ($column->getFixed()) {
				$rules[] = 'size:' . $column->getLength();
			} else {
				$rules[] = 'max:' . $column->getLength();
			}
		}

		return $rules;
	}
}