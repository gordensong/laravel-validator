<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class DateTimeType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		return [
			'date',
		];
	}
}