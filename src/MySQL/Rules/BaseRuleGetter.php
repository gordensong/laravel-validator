<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;
use GordenSong\Laravel\MySQL\RuleGetter;

abstract class BaseRuleGetter implements RuleGetter
{
	protected function getSchemaType(Column $column)
	{
		$schema = $column->getCustomSchemaOption('schema');

		return $schema['Type'];
	}
}