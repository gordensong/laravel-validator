<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class IntegerType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$schemaType = $this->getSchemaType($column);

		$rules = ['integer'];

		if (strpos($schemaType, 'mediumint') !== false) {
			if ($column->getUnsigned()) {
				$rules[] = 'min:0';
				$rules[] = 'max:16777215';
			} else {
				$rules[] = 'min:-8388608';
				$rules[] = 'max:8388607';
			}
		} else {
			if ($column->getUnsigned()) {
				$rules[] = 'min:0';
				$rules[] = 'max:4294967295';
			} else {
				$rules[] = 'min:-2147483648';
				$rules[] = 'max:2147483647';
			}
		}

//		if ($column->getNotnull()) {
//			array_unshift($rules, 'required');
//		}

		return $rules;
	}
}
