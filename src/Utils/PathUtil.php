<?php


namespace GordenSong\Laravel\Utils;


use Webmozart\Assert\Assert;

class PathUtil
{
	public static function crossPlatformPath(string $path)
	{
		Assert::notEmpty($path);

		return str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $path);
	}
}