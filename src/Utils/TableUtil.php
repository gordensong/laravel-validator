<?php

namespace GordenSong\Laravel\Utils;

use Doctrine\DBAL\Schema\Table;
use GordenSong\Laravel\Exceptions\TableNotExistException;

class TableUtil
{
	/**
	 * @throws TableNotExistException
	 */
	public static function load(string $connectionName, string $table): Table
	{
		DatabaseUtil::assertTableExist($connectionName, $table);

		$tableWithPrefix = DatabaseUtil::getPrefix($connectionName) . $table;

		return DatabaseUtil::schemaManager($connectionName)->listTableDetails($tableWithPrefix);
	}
}
