namespace App\Validators\{{$namespace}}\DatabaseRules;

trait {{$classname}}ValidatorTrait
{
    public function tableRules(): array
    {
        return [
@foreach($rules as $field => $rule)
            '{{$field}}' => [
@foreach($rule as $item)
                '{!! $item !!}',
@endforeach
            ],
@endforeach
        ];
    }
}
