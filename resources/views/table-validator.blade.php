namespace App\Validators\{{$namespace}};

class {{$classname}}Validator extends \GordenSong\Laravel\Support\Validator
{
    use \App\Validators\{{$namespace}}\DatabaseRules\{{$classname}}ValidatorTrait;

    public function customizeRules(): array
    {
        return [
@foreach($columns as $column)
            '{{$column->getName()}}' => [@if ($column->getNotnull())'required'@endif],
@endforeach
        ];
    }

    public function excludeRules(): array
    {
        return [
@foreach($carbonAt as $field)
            '{{$field}}',
@endforeach
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [
@foreach($columns as $column)
    // '{{$column->getName()}}' => '',
@endforeach
    ];

    protected $scenes = [

    ];
}
