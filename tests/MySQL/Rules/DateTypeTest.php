<?php


namespace Tests\MySQL\Rules;


use GordenSong\Laravel\MySQL\Rules\DateType;
use Illuminate\Database\QueryException;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DateTypeTest extends AbstractTypeRuleTest
{
	protected $class = DateType::class;

	public function test_date()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->date($this->field);
		});

		self::assertEquals(['date'], $this->getRules());

		DB::table($this->table)->insert([$this->field => '2015-01-01']);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals('2015-01-01', $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => '2015-01-01 10:00:00']);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals('2015-01-01', $value);
		DB::table($this->table)->truncate();

		DB::table($this->table)->insert([$this->field => '20150101']);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals('2015-01-01', $value);

		$validator = \Illuminate\Support\Facades\Validator::make(['date' => '20150101'], ['date' => 'date']);
		self::assertEquals(['date' => '20150101'], $validator->validate());
	}

	public function test_year()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->year($this->field);
		});

		self::assertEquals(['integer', 'between:1901,2155'], $this->getRules());

		DB::table($this->table)->insert([$this->field => '2015']);
		$value = DB::table($this->table)->value($this->field);
		self::assertEquals(2015, $value);

		try {
			DB::table($this->table)->insert([$this->field => '2156']);
			self::fail('year 2156 fail');
		} catch (QueryException $e) {
			self::assertStringContainsString('Numeric value out of range', $e->getMessage());
		}
	}
}
