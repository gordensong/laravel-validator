<?php


namespace Tests\MySQL\Rules;


use GordenSong\Laravel\MySQL\Rules\JsonType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class JsonTypeTest extends AbstractTypeRuleTest
{
	protected $class = JsonType::class;

	public function test_json()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->json($this->field);
		});

		self::assertSame(['json'], $this->getRules());

		$json = Validator::make([$this->field => json_encode(['a'])], [$this->field => 'json'])->validate();
		self::assertSame([$this->field => '["a"]'], $json);

		$json = Validator::make([$this->field => json_encode('a')], [$this->field => 'json'])->validate();
		self::assertSame([$this->field => '"a"'], $json);
	}

}
