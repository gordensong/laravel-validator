<?php


namespace Tests\MySQL\Rules;


use GordenSong\Laravel\MySQL\Rules\SmallIntType;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SmallIntTypeTest extends AbstractTypeRuleTest
{
	protected $class = SmallIntType::class;

	public function test_auto()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->smallIncrements($this->field = 'smallint');
		});

		self::assertEquals(['integer', 'min:0', 'max:65535'], $this->getRules());
	}

	public function test_smallInteger()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->smallInteger($this->field = 'smallint');
		});

		self::assertEquals(['integer', 'min:-32768', 'max:32767'], $this->getRules());
	}

	public function test_unsignedSmallInteger()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->id();
			$table->unsignedSmallInteger($this->field = 'smallint');
		});

		self::assertEquals(['integer', 'min:0', 'max:65535'], $this->getRules());
	}
}