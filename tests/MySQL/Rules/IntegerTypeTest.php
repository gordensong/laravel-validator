<?php


namespace Tests\MySQL\Rules;


use GordenSong\Laravel\MySQL\Rules\IntegerType;
use GordenSong\Laravel\Utils\TableMeta;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IntegerTypeTest extends AbstractTypeRuleTest
{
	protected $class = IntegerType::class;

	public function test_medium_auto()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->mediumIncrements($this->field = 'id');
		});
		$rules = $this->getRules();
		dump($rules);
		self::assertSame(['integer', 'min:0', 'max:16777215'], $rules);
	}

	public function test_medium_integer()
	{
		$signed = 'medium_integer';
		$unsigned = 'unsigned_medium_integer';

		Schema::create($this->table, function (Blueprint $table) use ($signed, $unsigned) {
			$table->tinyIncrements('id');

			$table->mediumInteger($signed);
			$table->unsignedMediumInteger($unsigned);
		});

		$meta = TableMeta::make($this->connection, $this->table);

		self::assertSame(['integer', 'min:-8388608', 'max:8388607'], $meta->getRule($signed));
		self::assertSame(['integer', 'min:0', 'max:16777215'], $meta->getRule($unsigned));
	}

	public function test_integer_pk()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments($this->field);
		});

		self::assertEquals(['integer', 'min:0', 'max:4294967295'], $this->getRules());
	}

	public function test_integer()
	{
		$signed = 'integer';
		$unsigned = 'unsigned_integer';

		Schema::create($this->table, function (Blueprint $table) use ($signed, $unsigned) {
			$table->tinyIncrements('id');

			$table->integer($signed);
			$table->unsignedInteger($unsigned);
		});

		$meta = TableMeta::make($this->connection, $this->table);

		self::assertSame(['integer', 'min:-2147483648', 'max:2147483647'], $meta->getRule($signed));
		self::assertSame(['integer', 'min:0', 'max:4294967295'], $meta->getRule($unsigned));
	}

	public function test_integer_nullable()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->integer($this->field = 'integer')->nullable(false);
		});

		self::assertEquals(['integer', 'min:-2147483648', 'max:2147483647'], $this->getRules());
	}

	public function test_year()
	{
		$year = 'year';

		Schema::create($this->table, function (Blueprint $table) use ($year) {
			$table->id('id');
			$table->year($year)->default(2021);
		});

		$meta = TableMeta::make($this->connection, $this->table);

		self::assertSame(['integer', 'between:1901,2155'], $meta->getRule($year));
	}
}
