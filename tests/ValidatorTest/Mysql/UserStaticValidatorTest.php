<?php

namespace Tests\ValidatorTest\Mysql;

use Tests\TestCase;
use Tests\Validators\Mysql\UserStaticValidator;

class UserStaticValidatorTest extends TestCase
{
	public function test_rules()
	{
		$validator = UserStaticValidator::instance();

		$rules = $validator->rules();

		self::assertCount(4, $rules);
		self::assertArrayHasKey('id', $rules);
		self::assertArrayHasKey('username', $rules);
		self::assertArrayHasKey('password', $rules);
		self::assertArrayHasKey('last_join_at', $rules);
	}
}