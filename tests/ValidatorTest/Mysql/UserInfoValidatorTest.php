<?php

namespace Tests\ValidatorTest\Mysql;

use GordenSong\Laravel\Exceptions\ValidatorFieldNotExistException;
use GordenSong\Laravel\Support\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Validators\Mysql\UserAddressValidator;
use Tests\Validators\Mysql\UserInfoValidator;
use Tests\Validators\Mysql\UserValidator;
use Tests\Validators\Validator\EmailValidator;
use Tests\Validators\Validator\QqValidator;

class UserInfoValidatorTest extends TestCase
{
	use RefreshDatabase;

	public function test_only()
	{
		$validator = UserInfoValidator::instance()->only('xing', 'ming');

		self::assertSame([
			"xing" => [
				"required", // added
				"string",
				"max:255",
			],
			"ming" => [
				"required", // added
				"string",
				"max:255",
			],
		], $validator->rules());

		try {
			UserInfoValidator::instance()->only('not_exist');
			self::fail('ValidatorFieldNotExistException');
		} catch (ValidatorFieldNotExistException $e) {
			self::assertStringContainsString('`not_exist`', $e->getMessage());
		}
	}

	public function test_scene()
	{
		$validator = UserInfoValidator::instance()->scene('edit-name', 'edit-age');
		dump($validator->rules());

		self::markTestIncomplete();
	}

	public function test_collection()
	{
		$collection = Collection::make([
			Collection::make([
				UserValidator::instance(),
				EmailValidator::instance(),
				QqValidator::instance()
			])->prefix('user'),
			UserInfoValidator::instance()->prefix('user_info')->exclude(['id', 'user_id']),
			UserAddressValidator::instance()->prefix('user_address.*')->exclude(['id', 'user_id']),
		]);
		dump($collection->rules());
		self::markTestIncomplete();
	}
}