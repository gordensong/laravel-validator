<?php

namespace Tests\Config;

use GordenSong\Laravel\Utils\PathUtil;
use Tests\TestCase;

class ConfigTest extends TestCase
{
	public function test_cache()
	{
		/** @var \Illuminate\Config\Repository $config */
		$config = app('config');
		$config->set('table-validator', [
			'cache' => [
				'path' => base_path('bootstrap/cache/table-validator'),
			],
		]);

		self::markTestIncomplete('config');
	}

	public function test_()
	{
		dump(storage_path('cache'));

		self::assertStringEndsWith(PathUtil::crossPlatformPath('/storage/cache'), storage_path('cache'));
	}
}