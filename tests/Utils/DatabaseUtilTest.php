<?php

namespace Tests\Utils;

use GordenSong\Laravel\Utils\DatabaseUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DatabaseUtilTest extends TestCase
{
	private $connection = 'mysql';

	public function test_getPrefix()
	{
		self::assertEquals('ims_', DatabaseUtil::getPrefix($this->connection));
	}

	public function test_allTableNames()
	{
		$allTableNames = DatabaseUtil::allTableNames($this->connection);
		dump($allTableNames);
		self::assertGreaterThan(1, $allTableNames);
	}

	public function test_tableExist()
	{
		self::assertTrue(DatabaseUtil::tableExist($this->connection, 'user'));
		self::assertFalse(DatabaseUtil::tableExist($this->connection, 'books'));
	}

	public function test_platform()
	{
		self::assertEquals('sqlite', DatabaseUtil::platform('testing'));
		self::assertEquals('mysql', DatabaseUtil::platform($this->connection));
	}
}
