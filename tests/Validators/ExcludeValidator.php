<?php

namespace Tests\Validators;

use GordenSong\Laravel\Support\Validator;

class ExcludeValidator extends Validator
{
	// simulate table rules
	protected $rules = [
		'title' => ['string'],
		'config' => ['string'],
		'created_at' => ['datetime'],
		'updated_at' => ['datetime'],
	];

	public function customizeRules(): array
	{
		return [
			'title' => ['string', 'required'],
			'config' => ['array'],
		];
	}

	public function excludeRules(): array
	{
		return [
			'config' => ['string'],
			'created_at',
			'updated_at',
		];
	}
}