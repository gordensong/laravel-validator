<?php

namespace Tests\Validators;

class ExcludeValidatorTest extends \Tests\TestCase
{
	public function test_exclude()
	{
		$validator = new ExcludeValidator();

		$rules = $validator->rules();

		dump($rules);

		self::assertSame([
			'title' => ['required', 'string'],
			'config' => ['array']
		], $rules);
	}
}