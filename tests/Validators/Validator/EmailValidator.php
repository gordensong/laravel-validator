<?php

namespace Tests\Validators\Validator;

use GordenSong\Laravel\Support\Validator;

class EmailValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'email' => [
				'required',
				'regex:/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i'
			]
		];
	}

	protected $messages = [
		'email.required' => '邮箱不能为空',
		'email.regex' => '邮箱格式不正确',
	];
}