<?php

namespace Tests\Validators\Validator;

use GordenSong\Laravel\Support\Validator;

class AddressValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'province' => ['string'],
			'city' => ['string'],
			'district' => ['string'],
			'street' => ['string'],
		];
	}

	protected $messages = [
		'province.required' => '省不能为空',
		'city.required' => '市不能为空',
		'district.required' => '区不能为空',
		'street.required' => '街道地址不能为空',
	];
}