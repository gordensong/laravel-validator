<?php

namespace Tests\Validators\Validator;

use GordenSong\Laravel\Support\Validator;

class QqValidator extends Validator
{
	public function customizeRules(): array
	{
		return [
			'qq' => ['numeric', 'min:10000']
		];
	}

	protected $messages = [
		'qq.required' => 'qq不能为空',
		'qq.regex' => 'QQ号码不正确',
	];
}