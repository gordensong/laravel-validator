<?php

namespace Tests\Validators;

use GordenSong\Laravel\Support\Collection;
use Tests\TestCase;
use Tests\Validators\Mysql\UserAddressValidator;
use Tests\Validators\Mysql\UserValidator;
use Tests\Validators\Validator\AddressValidator;
use Tests\Validators\Validator\EmailValidator;

class CollectionPrefixTest extends TestCase
{
	public function test_table_prefix()
	{
		$userValidator = UserValidator::instance()->prefix('users.*');
		dump($userValidator->rules());
		self::markTestSkipped();
	}

	public function test_table_collection_prefix()
	{
		$collection = Collection::make([
			UserValidator::instance(),
			UserAddressValidator::instance()->prefix('users.*'),
		]);
		dump($collection->rules());
	}

	public function test_entity_validator()
	{
		$collection = Collection::make([
			EmailValidator::instance(),
			AddressValidator::instance()->prefix('users.*'),
		]);
		dump($collection->rules());
	}
}