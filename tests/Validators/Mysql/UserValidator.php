<?php

namespace Tests\Validators\Mysql;

class UserValidator extends \GordenSong\Laravel\Support\TableValidator
{
	protected $connection = 'mysql';
	protected $table = 'user';

	public function customizeRules(): array
	{
		return [
			'id' => ['required'],
			'username' => ['required', 'max:30', 'min:3'],
			'password' => ['required'],
			'last_join_at' => [],
			'created_at' => [],
			'updated_at' => [],
			'deleted_at' => [],
		];
	}

	public function excludeRules(): array
	{
		return [
			'created_at',
			'updated_at',
			'deleted_at',
			'username' => ['min'],
		];
	}

	protected $messages = [

	];

	protected $attributes = [

	];

	protected $scenes = [
		'new' => ['username', 'password'],
		'edit-username' => ['id', 'username'],
		'edit-password' => ['id', 'password'],
		'edit-last-join' => ['id', 'last_join_at']
	];

}
