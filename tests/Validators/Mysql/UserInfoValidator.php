<?php

namespace Tests\Validators\Mysql;

class UserInfoValidator extends \GordenSong\Laravel\Support\TableValidator
{
	protected $connection = 'mysql';
	protected $table = 'user_info';

	public function customizeRules(): array
	{
		return [
			'id' => ['required'],
			'user_id' => ['required'],
			'xing' => [],
			'ming' => [],
			'age' => ['required'],
			'config1' => ['required'],
			'config2' => ['required'],
		];
	}

	public function excludeRules(): array
	{
		return [
		];
	}

	protected $messages = [

	];

	protected $attributes = [

	];

	protected $scenes = [
		'edit-name' => ['user_id', 'xing', 'ming'],
		'edit-age' => ['user_id', 'age'],
	];
}
