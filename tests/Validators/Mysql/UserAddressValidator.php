<?php

namespace Tests\Validators\Mysql;

class UserAddressValidator extends \GordenSong\Laravel\Support\TableValidator
{
    protected $connection = 'mysql';
    protected $table = 'user_address';

    public function customizeRules(): array
    {
        return [
            'id' => ['required'],
            'user_id' => ['required'],
            'province' => ['required'],
            'city' => ['required'],
            'district' => ['required'],
            'address' => ['required'],
            'created_at' => [],
            'updated_at' => [],
            'deleted_at' => [],
        ];
    }

    public function excludeRules(): array
    {
        return [
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [

    ];

    protected $scenes = [

    ];
}
