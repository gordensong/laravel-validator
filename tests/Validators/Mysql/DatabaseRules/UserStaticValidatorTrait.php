<?php

namespace Tests\Validators\Mysql\DatabaseRules;

trait UserStaticValidatorTrait
{
	protected function tableRules()
	{
		return [
			'id' => [
				'integer',
				'min:0',
			],
			'username' => [
				'string',
				'max:50',
			],
			'password' => [
				'string',
				'max:50',
			],
			'last_join_at' => [
				'date',
			],
			'created_at' => [
				'date',
			],
			'updated_at' => [
				'date',
			],
			'deleted_at' => [
				'date',
			],
		];
	}
}