<?php

namespace Tests\Validators\Mysql;



use Tests\Validators\Mysql\DatabaseRules\UserStaticValidatorTrait;

class UserStaticValidator extends \GordenSong\Laravel\Support\Validator
{
	use \Tests\Validators\Mysql\DatabaseRules\UserStaticValidatorTrait;

	public function customizeRules(): array
	{
		return [
			'id' => ['required'],
			'username' => ['required'],
			'password' => ['required'],
			'last_join_at' => [],
			'created_at' => [],
			'updated_at' => [],
			'deleted_at' => [],
		];
	}

	public function excludeRules(): array
	{
		return [
			'created_at',
			'updated_at',
			'deleted_at',
		];
	}

	protected $messages = [

	];

	protected $attributes = [
		// 'id' => '',
		// 'username' => '',
		// 'password' => '',
		// 'last_join_at' => '',
		// 'created_at' => '',
		// 'updated_at' => '',
		// 'deleted_at' => '',
	];

	protected $scenes = [

	];
}