<?php

namespace Tests\Support;

use GordenSong\Laravel\Support\EmptyTableValidator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EmptyTableValidatorTest extends TestCase
{
	use RefreshDatabase;

	/**
	 * @var \GordenSong\Laravel\Support\EmptyTableValidator
	 */
	private $validator;

	protected function setUp(): void
	{
		parent::setUp();

		$this->validator = new EmptyTableValidator('user');
	}

	public function test_dynamic_construct()
	{
		$rules = $this->validator->rules();

		self::assertArrayHasKey('username', $rules);
	}

	public function test_scene()
	{
		$this->validator->addScene('new', ['username']);

		$rules = $this->validator->scene('new')->rules();
		dump($rules);
		self::assertCount(1, $rules);
		self::assertArrayHasKey('username', $rules);
		self::assertArrayNotHasKey('password', $rules);
	}
}