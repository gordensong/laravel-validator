<?php

namespace Tests\Support;

use GordenSong\Laravel\Support\EmptyValidator;
use Tests\TestCase;

class SceneTest extends TestCase
{
	/**
	 * @var \GordenSong\Laravel\Support\EmptyValidator
	 */
	private $validator;

	protected function setUp(): void
	{
		parent::setUp();

		$this->validator = new EmptyValidator();
		$this->validator->setRules([
			'id' => ['integer'],
			'title' => ['string'],
			'price' => ['numeric',],
		]);
		$this->validator->setScenes([
			'create' => ['title' => 'required', 'price' => ['required']],
			'update' => ['id' => 'required', 'price' => 'required'],
		]);
	}

	public function test_rules()
	{
		self::assertEquals([
			'id' => ['integer'],
			'title' => ['string'],
			'price' => ['numeric',],
		], $this->validator->rules());
	}

	public function test_getScene()
	{
		$scene = $this->validator->getScene('create');

		dump($scene);

		self::assertSame([
			'title' => ['required'],
			'price' => ['required'],
		], $scene);
	}

	public function test_getScenes()
	{
		self::assertEquals([
			'create' => ['title' => ['required'], 'price' => ['required']],
			'update' => ['id' => ['required'], 'price' => ['required']],
		], $this->validator->getScenes());
	}

	public function test_scene()
	{
		$rules = $this->validator->scene('create')->rules();

		dump($rules);

		self::assertEquals([
			'title' => ['required', 'string'],
			'price' => ['required', 'numeric'],
		], $rules);
	}

	public function test_scene_many_scene()
	{
		$rules = $this->validator->scene('create', 'update')->rules();

		ksort($rules);

//		dump($rules);
		self::assertEquals([
			'id' => ['required', 'integer'],
			'price' => ['required', 'numeric'],
			'title' => ['required', 'string'],
		], $rules);
	}
}