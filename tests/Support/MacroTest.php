<?php

namespace Tests\Support;

use GordenSong\Laravel\Support\Collection;
use GordenSong\Laravel\Support\Validator;
use Illuminate\Validation\Factory;
use Tests\TestCase;
use Tests\Validators\Mysql\UserValidator;

class MacroTest extends TestCase
{
	public function test_validator()
	{
		Validator::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validator = UserValidator::instance()->except(['id']);
		dump($validator->rules());
		$validated = $validator->validate($data);

		self::assertSame($data, $validated);
		dump('validated', $validated);
	}

	public function test_Collection()
	{
		Collection::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validated = Collection::make([UserValidator::instance()->except(['id'])])->validate($data);

		self::assertSame($data, $validated);
	}
}