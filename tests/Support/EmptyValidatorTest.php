<?php

namespace Tests\Support;

use GordenSong\Laravel\Support\EmptyValidator;
use Illuminate\Validation\Factory;
use Tests\TestCase;

class EmptyValidatorTest extends TestCase
{
	public function test_prefix()
	{
		$ev = new EmptyValidator();
		$ev->setRules([
			'id' => ['integer'],
			'name' => ['string'],
			'age' => ['integer'],
		]);

		$rules = $ev->prefix('base.name', false)->rules();

		self::assertEquals([
			'base' => ['array'],
			'base.name' => ['array'],
			'base.name.id' => ['integer'],
			'base.name.name' => ['string'],
			'base.name.age' => ['integer'],
		], $rules);

		$post = [
			'base' => [
				'name' => [
					'id' => 1,
					'name' => 'name',
					'age' => 18,
				],
			]
		];

		/** @var Factory $factory */
		$factory = app(Factory::class);
		$data = $factory->validate($post, $rules);

		self::assertEquals($post, $data);
	}

	public function test_scene()
	{
		$ev = new EmptyValidator();
		$ev->setRules(['id' => ['integer'], 'name' => ['string'], 'age' => ['integer']]);
		$ev->addScene('name', ['id', 'name']);

		self::assertEquals([
			'id' => ['required', 'integer'],
			'name' => ['required', 'string'],
		], $ev->scene('name')->rules());
	}

	public function test_scene2()
	{
		$ev = new EmptyValidator();
		$ev->setRules(['id' => ['integer'], 'name' => ['string'], 'age' => ['integer']]);
		$ev->addScene('age', ['id', 'age' => 'required']);

		$rules = $ev->scene('age')->rules();

		self::assertEquals([
			'id' => ['required', 'integer'],
			'age' => ['required', 'integer'],
		], $rules);
	}

	public function test_scene_merge_rule_order()
	{
		$ev = new EmptyValidator();
		$ev->setRules(['id' => ['integer'], 'name' => ['string'], 'age' => ['integer', 'max:128']]);
		$ev->addScene('age', ['id', 'age' => ['required', 'min:0', 'max:150']]);

		$rules = $ev->scene('age')->rules();

		self::assertEquals([
			'id' => ['required','integer'],
			'age' => ['required', 'integer', 'max:150', 'min:0'],
		], $rules);
	}
}