<?php

namespace Tests\Support;


use GordenSong\Laravel\Support\Collection;
use GordenSong\Laravel\Support\Validator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\Factory;
use Tests\TestCase;
use Tests\Validators\Mysql\UserAddressValidator;
use Tests\Validators\Mysql\UserInfoValidator;
use Tests\Validators\Mysql\UserValidator;

class CollectionTest extends TestCase
{
	use RefreshDatabase;

	public function test_make()
	{
		$collection = Collection::make([new UserValidator(), new UserInfoValidator()]);

		self::assertTrue($collection->hasField('username'));
		self::assertTrue($collection->hasField('xing'));
	}

	public function test_add()
	{
		$collection = Collection::make()->add(new UserValidator);
		self::assertTrue($collection->hasField('username'));

		$collection->add(new UserInfoValidator());
		self::assertTrue($collection->hasField('xing'));
		self::assertTrue($collection->hasField('ming'));
	}

	public function test_prefix()
	{
		$validateCollection = Collection::make()
			->add(UserValidator::instance())
			->prefix('data');

		$rules = $validateCollection->rules();

		self::assertArrayHasKey('data.username', $rules);
		self::assertArrayHasKey('data.password', $rules);
	}

	public function test_macro()
	{
		Validator::macro('validate', function (array $data) {
			/** @var Factory $factory */
			$factory = app(Factory::class);

			/** @var Validator $this */
			return $factory->validate($data, $this->rules(), $this->messages(), $this->attributes());
		});

		$data = [
			'username' => 'abc',
			'password' => '123',
		];
		$validated = (new UserValidator())->except('id')->validate($data);
		dump($validated);

		self::assertEquals($data, $validated);
	}

	public function test_nest()
	{
		$collection = Collection::make([
			UserValidator::instance()->prefix('author'),
			Collection::make([
				UserAddressValidator::instance()->prefix('address.*'),
			]),
		])->prefix('base');

		$rules = $collection->rules();

		self::assertArrayHasKey('base.author', $rules);
		self::assertArrayHasKey('base.address.*', $rules);

		dump($rules);
	}

	public function test_key_prefix()
	{
		$collection = Collection::make([
			'user' => UserValidator::instance(),
			'userinfo' => UserInfoValidator::instance(),
			'address.*' => UserAddressValidator::instance(),
		]);

		$rules = $collection->rules();

		self::assertArrayHasKey('user', $rules);
		self::assertArrayHasKey('userinfo', $rules);
		self::assertArrayHasKey('address', $rules);
		self::assertArrayHasKey('address.*', $rules);

		dump($rules);
	}
}
