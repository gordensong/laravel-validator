<?php


namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
	use RefreshDatabase;

	protected function setUp(): void
	{
		parent::setUp();

		$this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
	}

	protected function getEnvironmentSetUp($app)
	{
		config(['database.connections.mysql.prefix' => 'ims_']);
	}
}
